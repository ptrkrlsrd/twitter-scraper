module gitlab.com/ptrkrlsrd/twitter-scraper.git

go 1.16

require (
	github.com/dghubble/go-twitter v0.0.0-20210609183100-2fdbf421508e
	github.com/dghubble/oauth1 v0.7.0
	github.com/gin-gonic/gin v1.7.2
	github.com/gorilla/websocket v1.4.2 // indirect
	gopkg.in/olahol/melody.v1 v1.0.0-20170518105555-d52139073376
)
