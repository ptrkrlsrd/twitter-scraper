package main

import (
	"fmt"
	"log"
	"os"
	"os/signal"
	"syscall"

	"github.com/dghubble/go-twitter/twitter"
	"github.com/dghubble/oauth1"
	"gitlab.com/ptrkrlsrd/twitter-scraper.git/pkg/server"
	"gopkg.in/olahol/melody.v1"
)

func NewClient(consumerID string, consumerSecret string, accessID string, accessSecret string) *twitter.Client {
	config := oauth1.NewConfig(consumerID, consumerSecret)
	token := oauth1.NewToken(accessID, accessSecret)
	httpClient := config.Client(oauth1.NoContext, token)

	twitterClient := twitter.NewClient(httpClient)
	return twitterClient
}

func main() {
	// Get environment variables
	consumerID := os.Getenv("CONSUMER_ID")
	consumerSecret := os.Getenv("CONSUMER_SECRET")
	accessID := os.Getenv("ACCESS_ID")
	accessSecret := os.Getenv("ACCESS_SECRET")
	port := os.Getenv("TWEET_SERVER_PORT")
	if port == "" {
		port = ":5000"
	}

	// Get the topic or return an error
	topic := os.Getenv("TWEET_SERVER_TOPIC")
	if topic == "" {
		log.Panic(fmt.Errorf("no topic found. Please enter a topic through setting the TWEET_SERVER_TOPIC env variable"))
	}

	// Create a Twitter client
	twitterClient := NewClient(consumerID, consumerSecret, accessID, accessSecret)
	tweetChan := make(chan (*twitter.Tweet)) // Make a channel which will receive tweets

	// Setup the websockets server
	ws := melody.New()
	// Create a http server with the websockets server
	httpServer := server.New(ws)
	go httpServer.Run(port)                          // Start the http server asynchronously
	go server.SubscribeToTweetChannel(tweetChan, ws) // Subscribe to the TweetChannel asynchronously

	filterParams := &twitter.StreamFilterParams{
		Track:         []string{topic},
		StallWarnings: twitter.Bool(true),
	}

	stream, err := twitterClient.Streams.Filter(filterParams)
	if err != nil {
		log.Fatal(err)
	}

	// Demux is used to filter tweets from other events sent from the streaming endpoint
	demux := twitter.NewSwitchDemux()
	demux.Tweet = func(tweet *twitter.Tweet) {
		tweetChan <- tweet
	}
	go demux.HandleChan(stream.Messages) // Start listening for tweets

	ch := make(chan os.Signal)
	signal.Notify(ch, syscall.SIGINT, syscall.SIGTERM)
	log.Println(<-ch)

	fmt.Println("Stopping Stream...")
	stream.Stop()
}
