package server

import (
	"encoding/json"
	"fmt"

	"github.com/dghubble/go-twitter/twitter"
	"github.com/gin-gonic/gin"
	"gopkg.in/olahol/melody.v1"
)

func New(m *melody.Melody) *gin.Engine {
	r := gin.Default()

	m.HandleMessage(func(s *melody.Session, msg []byte) {
		m.Broadcast(msg)
	})

	r.GET("/ws", func(c *gin.Context) {
		m.HandleRequest(c.Writer, c.Request)
	})

	return r
}

func SendWebsocketsMessage(t *twitter.Tweet, ws *melody.Melody) error {
	msg, err := json.Marshal(t)
	if err != nil {
		return err
	}

	err = ws.Broadcast(msg)
	if err != nil {
		return err
	}
	return nil
}

func SubscribeToTweetChannel(tweetChan chan *twitter.Tweet, ws *melody.Melody) error {
	for {
		t := <-tweetChan
		if err := SendWebsocketsMessage(t, ws); err != nil {
			return err
		}

		fmt.Printf("%s\n\t%s\n\n", t.User.ScreenName, t.Text)
	}
}
