# Twitter Stream

## Setup
* Create a `.env` file in `./server/` with the following content: 
```
CONSUMER_ID=""
CONSUMER_SECRET=""
ACCESS_ID=""
ACCESS_SECRET=""
GIN_MODE=release
TWEET_SERVER_PORT=":5000"
TWEET_SERVER_TOPIC="coop"
````

* Run docker-compose up --build
* Visit localhost:3000 to see the feed
