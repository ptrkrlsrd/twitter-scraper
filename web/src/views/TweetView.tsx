import fetchTweets from '../actions/fetchTweets'
import { useEffect } from 'react'
import Tweet from '../components/Tweet'

const TwitterView = (props)=> {
  useEffect(()=> {
      fetchTweets(props.dispatch)
  }, [])

  return (
    <div>
      <ul className="tweet--list">
          {props.store.tweets.map((i) => {
            return <Tweet key={i.id} tweet={i} />
          })}
      </ul>
    </div>
  )
}

export default TwitterView
