interface State  {
  isFetchingTweets: boolean
  tweets: string[]
  error: string | undefined
}

const initialState = { 
  tweets: [], 
  isFetchingTweets: false,
  error: undefined
} as State

function reducer(state: State, action) {
  switch(action.type) {
    case 'SET_TWEETS':
      return { ...state, tweets: [...state.tweets, action.newTweet] }
    case 'IS_FETCHING_TWEETS':
      return { ...state, isFetchingTweets: action.value }
    case 'ERROR':
      return { ...state, error: action.error }
    default:
      return state
  }
}

export { initialState, reducer }
