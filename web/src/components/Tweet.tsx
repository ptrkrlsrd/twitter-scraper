const Tweet = (props) => {
  const { tweet } = props
  return (
    <li key={tweet.id}>
      <h1>{tweet.user.screen_name}</h1>
      <span>{tweet.text}</span>
    </li>
  )
}

export default Tweet
