import { useReducer } from 'react'
import { BrowserRouter as Router, Switch, Route, Link } from 'react-router-dom';
import { reducer, initialState } from './store/index'
import TwitterView from './views/TweetView'
import './App.css'

function App() {
  const [store, dispatch] = useReducer(reducer, initialState)

  return (
    <Router>
      <Switch>
        <Route exact path="/">
          <TwitterView store={store} dispatch={dispatch} />
        </Route>
      </Switch>
    </Router>
  )
}

export default App
