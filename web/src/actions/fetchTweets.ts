import { Dispatch } from 'redux';

const fetchTweets = async (dispatch: Dispatch) => {
  dispatch({ type: 'IS_FETCHING_TWEETS', value: true })

  try {
    const socket = new WebSocket('ws://localhost:5000/ws');

    socket.addEventListener('message', (event) => {
      const json = JSON.parse(event.data);
      dispatch({ type: 'SET_TWEETS', newTweet: json }) 
    });

    dispatch({ type: 'IS_FETCHING_TWEETS', value: false })
  } catch(error) {
    dispatch({ type: 'GET_TWEETS_ERROR', error: error })
  }
}

export default fetchTweets
